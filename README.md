# unity_fractals
A simple 2D fractal renderer with interactable dynamic vertices

![fractal_renderer](images/fractal_example.png)