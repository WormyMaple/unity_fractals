using System.Collections.Generic;

// Unity Engine created by Unity Technologies
using UnityEngine;
using UnityEngine.UI;

public class VertexCreator : MonoBehaviour
{
    [SerializeField] FractalGenerator fractalGenerator;

    [SerializeField] Transform pointer, origin;
    [SerializeField] List<Transform> points;
    [SerializeField] GameObject pointPrefab;

    [SerializeField] Vector2 pointerMaxPos;
    [SerializeField] float pointerSpeed;

    [SerializeField] Camera MainCamera;
    RectTransform pointerRect;
    Vector2 originPos;

    void Start()
    {
        pointerRect = pointer.GetComponent<RectTransform>();
        originPos = origin.GetComponent<RectTransform>().position;

        for (int i = 0; i < points.Count; i += 1) points[i].GetComponent<RectTransform>().anchoredPosition = fractalGenerator.fractalDirs[i] * pointerMaxPos;

        DrawVertexLines(points, originPos);
    }

    void Update()
    {
        Vector2 moveDir = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (moveDir != Vector2.zero) pointer.position += (Vector3)moveDir * pointerSpeed * Time.deltaTime;

        // Pointer containment
        if (Mathf.Abs(pointerRect.anchoredPosition.x) > pointerMaxPos.x) pointerRect.anchoredPosition = new Vector2(pointerMaxPos.x * Mathf.Sign(pointerRect.anchoredPosition.x), pointerRect.anchoredPosition.y);
        if (Mathf.Abs(pointerRect.anchoredPosition.y) > pointerMaxPos.y) pointerRect.anchoredPosition = new Vector2(pointerRect.anchoredPosition.x, pointerMaxPos.y * Mathf.Sign(pointerRect.anchoredPosition.y));

        bool PointSelected = false;

        // Loop through points to find interaction
        foreach (Transform point in points)
        {
            // If pointer is within a point's radius
            if (Vector2.Distance(pointer.position, point.position) < 10)
            {
                point.GetComponent<Image>().color = Color.white; // Change color of pointer if hovered over

                if (Input.GetKey(KeyCode.Space))
                {
                    point.position = pointer.position; // Match point's position to pointer

                    // Draw new updated fractal
                    fractalGenerator.fractalDirs[points.IndexOf(point)] = point.GetComponent<RectTransform>().anchoredPosition / pointerMaxPos;
                    fractalGenerator.GenerateFractal(fractalGenerator.fractalDirs, Vector2.zero, fractalGenerator.fractalIterations);

                    DrawVertexLines(points, originPos);

                    PointSelected = true;

                    break;
                }
            }
            else point.GetComponent<Image>().color = Color.blue;
        }

        if (!PointSelected && Input.GetKeyDown(KeyCode.Space))
        {
            Transform newPoint = Instantiate(pointPrefab, pointer.position, Quaternion.identity, transform).transform;

            points.Add(newPoint);
            fractalGenerator.fractalDirs.Add(Vector3.zero);
        }
    }

    void DrawVertexLines(List<Transform> points, Vector2 originPoint)
    {
        foreach (Transform point in points)
        {
            point.GetComponent<LineRenderer>().SetPositions(new Vector3[] { (Vector2)MainCamera.ScreenToWorldPoint(originPoint), (Vector2)MainCamera.ScreenToWorldPoint(point.GetComponent<RectTransform>().position) });
        }
    }
}
