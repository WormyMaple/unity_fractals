using System.Collections.Generic;

// Unity Engine created by Unity Technologies
using UnityEngine;

public class FractalGenerator : MonoBehaviour
{
    public List<Vector3> fractalDirs = new List<Vector3> { new Vector3(0.5f, 0.5f, -45), new Vector3(-0.5f, 0.5f, 45) };
    [SerializeField] GameObject fractalParent, fractalLine;
    [SerializeField] float width, drawDelay;
    [Range(0, 10)] public int fractalIterations;
    float originalWidth;

    void Start()
    {
        originalWidth = width; // Initialize reference width

        // Draw starting fractal
        GenerateFractal(fractalDirs, Vector2.zero, fractalIterations, true);
    }

    public void GenerateFractal(List<Vector3> localFractalDirs, Vector2 startPoint, int iterations, bool initialFractal = false)
    {
        width = originalWidth; // Restore width

        foreach (GameObject toDestroy in GameObject.FindGameObjectsWithTag("FractalParent")) Destroy(toDestroy); // Delete all prior 

        // Initialize point lists
        List<Vector3> prevPoints = new List<Vector3>() { startPoint };
        List<Vector3> tempPoints = new List<Vector3>();

        if (!initialFractal) // Do not compute rotations if this is the inital fractal
        {
            // Get rotations of points
            for (int i = 0; i < localFractalDirs.Count; i += 1)
            {
                float rot = -Mathf.Atan2(localFractalDirs[i].x, localFractalDirs[i].y) * 180 / Mathf.PI;
                localFractalDirs[i] = new Vector3(localFractalDirs[i].x, localFractalDirs[i].y, rot);
            }

            fractalDirs = localFractalDirs;
        }

        for (int i = 0; i < iterations - fractalDirs.Count; i += 1) // Iteration loop
        {
            // Clear temp points
            if (i != 0) prevPoints = tempPoints;
            tempPoints = new List<Vector3>();

            foreach (Vector3 point in prevPoints)
            {
                Transform newParent = Instantiate(fractalParent, (Vector2)point, Quaternion.identity).transform;

                foreach (Vector3 fractalDir in fractalDirs)
                {
                    // Create new line
                    Transform newLine = Instantiate(fractalLine, newParent).transform;

                    // Rotate vector
                    Vector2 rotatedDir = Quaternion.Euler(new Vector3(0, 0, point.z)) * fractalDir;

                    // Create next iteration point
                    Vector3 tempPoint = (Vector2)newLine.position + (rotatedDir * width);
                    tempPoints.Add(new Vector3(tempPoint.x, tempPoint.y, fractalDir.z + point.z));

                    // Set line positions between old iteration anchor and new iteration anchor
                    LineRenderer line = newLine.GetComponent<LineRenderer>();
                    line.SetPositions(new Vector3[] { newParent.position, tempPoint });
                }
            }

            width /= 2; // Divide width for next iteration
        }
    }
}
